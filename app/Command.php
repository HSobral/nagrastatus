<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Command extends Model
{
    /**
     * Get commands by status
     *
     * @param string $status
     * @return array
     */
    public static function getCommandByStatus($status){
        return DB::select( 
            DB::raw("SELECT 
                        * 
                    FROM 
                        ngr 
                    WHERE 
                        estado = '".$status."' 
                    ORDER BY idngr DESC limit 100 "));
    }

    /**
     * Get info about all commands
     *
     * @return array|null 
     */
    public static function getInfoAllCommands(?array $statusNotIncluded = null){
        $sql = "SELECT 
                    COUNT(*) AS total, MAX(datacriado) AS data, estado
                FROM 
                    ngr";

        if (!empty($statusNotIncluded)) {
            $statusParams = implode("','", $statusNotIncluded);
            $sql .= " WHERE estado NOT IN ('$statusParams') ";
        }
        $sql .= " GROUP BY estado";

        return DB::select(DB::raw($sql));
    }

    /**
     * Get commands in status queue
     *
     * @param string $connection
     * @return array
     */
    public static function getCommandsQueued($connection){
        return  DB::connection($connection)->select( 
            DB::raw("SELECT 
                        COUNT(*) AS total, MAX(datacriado) AS data, estado 
                    FROM 
                        ngr 
                    WHERE 
                        estado = 'queue' 
                    ORDER BY idngr DESC"));
    }

    /**
     * Get last commands with state confirmado
     *
     * @param string      $connection
     * @param array       $status
     * @param string|null $beginDate
     * @param string|null $endDate
     * @return array
     */
    public static function getCommandsyInterval(string $connection, ?array $status = null, ?string $beginDate = null, ?string $endDate = null){
        $beginDate = $beginDate ?? date("Y-m-d H:i:s", strtotime("-1 hour"));
        $endDate   = $endDate ?? date('Y-m-d H:i:s');

        $sql = "SELECT 
                    COUNT(*) AS total, MAX(datacriado) AS data, estado
                FROM 
                    ngr 
                WHERE 
                    datacriado BETWEEN '" . $beginDate . "' AND '" . $endDate . "'";

        if (!empty($status)) {
            $statusParams = implode("','", $status);
            $sql .= " AND estado IN ('$statusParams') ";
        }
        $sql .= " GROUP BY estado";

        return DB::connection($connection)->select(DB::raw($sql));
    }

    /**
     * Get last commands for draw graphic
     *
     * @param string      $connection
     * @param string      $estado
     * @param string|null $beginDate
     * @param string|null $endDate
     * @return array
     */
    public static function getCommandsGraphByInterval(string $connection, ?string $beginDate = null, ?string $endDate = null) {
        $beginDate = $beginDate ?? date("Y-m-d H:i:s", strtotime("-1 hour"));
        $endDate   = $endDate ?? date('Y-m-d H:i:s');

        return DB::connection($connection)->select(
            DB::raw("SELECT 
                        estado,
                        DATE_FORMAT(datacriado,'%d/%m/%Y %H:%i') AS datacriado,
                        COUNT(*) AS total
                     FROM 
                        ngr
                     WHERE 
                        datacriado BETWEEN '".$beginDate."' AND '".$endDate."'
                     GROUP BY DATE_FORMAT(datacriado,'%d/%m/%Y %H:%i'), estado"));
    }
}