<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Command as CommandModel;

class GraphicObject
{
    public $name = "";
    public $data = [];
}

class DashboardController extends Controller
{
    // Match colors to series by status
    public static $colorsStatus = [
        "confirmado" => "#38c172",
        "enviado"    => "#00adff",
        "queue"      => "#ffed4a",
        "erro"       => "#e3342f",
        "activo"     => "#343a40",
        "postponed"  => "#1c1d1f",
        "aprocessar" => "#0a07ff",
        "rejected"   => "#ff5722",
        "tmp"        => "#9e9e9e",
    ];

    public function index() {
        $commandsAngola      = [];
        $commandsVendasNovas = [];
        $commandGraphic      = [];
        $result              = [];
        $resutlAggregate     = [];

        $commandGraph = CommandModel::getCommandsGraphByInterval('mysqlVN');

        foreach ($commandGraph as $command) {
            $convertTimeStamp                                    = (int) date_create_from_format('d/m/Y H:i', $command->datacriado)->format('U') * 1000; // convert time to timestamp
            $commandGraphic[$command->estado][$convertTimeStamp] = $command->total;
        }

        //create information for the series of each status
        foreach ($commandGraphic as $estado => $value) {
            $result[$estado]["name"]  = $estado;
            $result[$estado]["data"]  = [];
            $result[$estado]["color"] = self::$colorsStatus[$estado] ?? "#212929";
            foreach ($value as $timestamp => $total) {
                $line = array($timestamp, $total);
                $result[$estado]["data"][] = $line;
            }
        }

        //Agregate all series
        foreach ($result as $value) {
            $resutlAggregate[] =  $value;
        }

        $commandGraphic = json_encode($resutlAggregate);

        $infoComandosVN = CommandModel::getInfoAllCommands(['confirmado', 'erro', 'postponed', 'enviado','rejected']); // Get info commands except status (Confirmado and Erro)
        $commandsVNLast = CommandModel::getCommandsyInterval('mysqlVN', ['confirmado', 'erro', 'postponed', 'enviado', 'rejected']); // Get info commands with determinate status

        $commandsAngolaLastHourConfirm = CommandModel::getCommandsyInterval('mysqlAO', ['confirmado']); // Get info commands with status (confirmado)
        $commandsAngolaQueued          = CommandModel::getCommandsQueued('mysqlAO'); // Get all commands in status Queue

        $commandsAngola      = array_merge($commandsAngolaLastHourConfirm, $commandsAngolaQueued); // Merge all information for Angola Table
        $commandsVendasNovas = array_merge($infoComandosVN, $commandsVNLast); // Merge all information for Vendas Novas Table

        //Create Alerts for determinate status
        $alertMessages = [];
        foreach ($commandsAngola as $command) {
            if ($command->estado == 'confirmado' && $command->total > 0) {
                $alertMessages[] = [
                    "type"    => "danger",
                    "message" => "Problemas a processar comandos no estado confirmado"
                ];
            } elseif ($command->estado == 'queue' && $command->total > 1) {
                $alertMessages[] = [
                    "type"    => "danger",
                    "message" => "Problemas a processar comandos no estado Queue"
                ];
            }
        }

        return view('pages.dashboard', compact('commandsAngola', 'commandsVendasNovas', 'commandGraphic', 'alertMessages'));
    }
}
