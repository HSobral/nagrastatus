<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Command as CommandModel;

class CommandController extends Controller
{
    
    public function getCommandByStatus($estado)
    {
        $commandQueued = CommandModel::getCommandByStatus($estado);
        return response()->json($commandQueued, 200);
    }

    public function getInfoAllCommands()
    {
        $commandInfo = CommandModel::getInfoAllCommands();
        return response()->json($commandInfo, 200);
    }

    public function getLastCommandConfirm()
    {
        $lastCommandsConfirm = CommandModel::getCommandsConfimByInterval('mysqlAO');
        return response()->json($lastCommandsConfirm, 200);
    }
    
}
