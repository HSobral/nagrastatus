@extends('layouts.index')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 box">
                <div class="dashboard">
                    <div class="header">
                        <div class="box-title">Angola Status</div>
                    </div>
                    <div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Total</th>
                                    <th>Última data </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($commandsAngola as $command)
                                @if(($command->estado == 'queue' && $command->total > 500) || ($command->estado == 'confirmado' && $command->total == 0))
                                <tr class="alert-error">
                                    @else
                                <tr>
                                    @endif
                                    <td>{{strtoupper($command->estado)}}</td>
                                    <td>{{$command->total}}</td>
                                    <td>{{$command->data}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6 box">
                <div class="dashboard">
                    <div class="header">
                        <div class="box-title">Vendas Novas Status</div>
                    </div>
                    <div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Total</th>
                                    <th>Última data </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($commandsVendasNovas as $command)
                                @if(($command->estado == 'queue' && $command->total > 500) || ($command->estado == 'confirmado' && $command->total == 0))
                                <tr class="alert-error">
                                    @else
                                <tr>
                                    @endif
                                    <td>{{strtoupper($command->estado)}}</td>
                                    <td>{{$command->total}}</td>
                                    <td>{{$command->data}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 margin-btm-70 template-charts">
        <div id="graphCommands" data-info='<?php echo $commandGraphic; ?>'></div>
    </div>
</div>
@endsection
@section('javascripts')
<script type="text/javascript">
    // Remove flash messages    
    $(".flash-message").fadeTo(10000, 1000).slideUp(1000, function() {
        $(".flash-message").slideUp(1000);
    });

    let infoCommands = $('.template-charts #graphCommands').data('info'); // Get info for draw graph

    let graphCommands = $('.template-charts #graphCommands').highcharts('StockChart', {
        chart: {
            zoomType: 'x'
        },
        title: {
            text: 'Status dos comandos Nagra Vendas Novas'
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Minuto'
            },
            type: 'datetime',

            labels: {
                format: '{value:%H:%M:%S}'
            }
        },
        yAxis: [{
            opposite: false,
            title: {
                enabled: true,
                text: 'Nº de comandos'
            },
        }],
        tooltip: {
            formatter: function() {
                let date = new Date(this.x).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                return '<span style="color:' + this.points[0].series.color + '">' + this.points[0].series.name + '</span>: ' + this.y + '<br>' + date;
            },
        },
        rangeSelector: {
            selected: 4,
            inputEnabled: false,
            buttonTheme: {
                visibility: 'hidden'
            },
            labelStyle: {
                visibility: 'hidden'
            }
        },
        plotOptions: {
            series: {
                connectNulls: true
            }
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: true,
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0,
            labelFormatter: function() {
                let legendName = this.name;
                let n = legendName.indexOf("|");

                if (n != '-1') {
                    let legendFinal = legendName.slice(0, n) + "<br>" + legendName.slice(n);
                    return legendFinal;
                }
                return legendName;

            }
        },
        series: infoCommands
    });

</script>
@stop