<!doctype html>
<html class="no-js" lang="">
@include('partials.head')

<body>
	@include('partials.navbar')
	<div class="flash-message">
        @include('partials.flash-message')
	</div>
	<div class="container-fluid">@yield('content')</div>
	@include('partials.footer')
</body>

</html>