<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Nagra Status </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="60" />
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href={{ URL::asset('css/bootstrap.min.css')}}  rel="stylehseet">
    <link href={{ URL::asset('css/main.css')}}  rel="stylesheet">
    <link href={{ URL::asset('css/app.css')}}  rel="stylesheet">
    @yield('stylesheets')
</head>
