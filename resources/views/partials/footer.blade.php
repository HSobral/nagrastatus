<footer class="footer">
    <div class="container">
        <span class="produced">Nagra© - {{date('Y')}}</span>
    </div>
</footer>
<!--JS -->
<script type="text/javascript" src="{{ URL::asset('js/jquery-3.4.1.slim.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/highcharts/highstock.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/highcharts/highcharts-more.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/highcharts/exporting.js') }}"></script>
@yield('javascripts')