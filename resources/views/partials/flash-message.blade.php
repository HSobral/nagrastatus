@foreach ($alertMessages as $message)
    @if(!empty( $message['type']) && !empty( $message['message']))
        <p class="alert alert-{{ $message['type'] }}">{{ $message['message']}}</p>
    @endif
@endforeach